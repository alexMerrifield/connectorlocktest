/**
 * Auth
 *
 * @module      :: Model
 * @description :: Holds all authentication methods for a User
 */

module.exports = {

  attributes: require('connectorLock').models.auth.attributes({
    
    /* e.g.
    nickname: 'string'
    */
    
  }),
  
  beforeCreate: require('connectorLock').models.auth.beforeCreate,
  beforeUpdate: require('connectorLock').models.auth.beforeUpdate
};
