/**
 * Use
 *
 * @module      :: Model
 * @description :: Tracks the usage of a given Jwt
 */

module.exports = {

  attributes: require('connectorLock').models.use.attributes({
    
    /* e.g.
    nickname: 'string'
    */
    
  })
};
