/**
 * User
 *
 * @module      :: Model
 * @description :: This is the base user model
 */

module.exports = {

  attributes: require('connectorLock').models.user.attributes({
    
    /* e.g.
    nickname: 'string'
    */
    
  }),
  
  beforeCreate: require('connectorLock').models.user.beforeCreate,
  beforeUpdate: require('connectorLock').models.user.beforeUpdate
};
