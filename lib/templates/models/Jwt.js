/**
 * Jwt
 *
 * @module      :: Model
 * @description :: Holds all distributed json web tokens
 */

module.exports = {

  attributes: require('connectorLock').models.jwt.attributes({
    
    /* e.g.
    nickname: 'string'
    */
    
  })
};
