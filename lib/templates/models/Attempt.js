/**
 * Attempt
 *
 * @module      :: Model
 * @description :: Tracks login attempts of users on your app.
 */

module.exports = {

  attributes: require('connectorLock').models.attempt.attributes({
    
    /* e.g.
    nickname: 'string'
    */
    
  })
};