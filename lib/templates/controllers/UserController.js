/**
 * UserController.js 
 * 
 * @module      :: Controller
 * @description :: Provides the base user
 *                 actions used to make connectorLock work.
 */

module.exports = require('connectorLock').actions.user({
  /* e.g.
    action: function(req, res){
  
    }
  */
});