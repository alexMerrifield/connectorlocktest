/**
 * AuthController
 *
 * @module      :: Controller
 * @description	:: Provides the base authentication
 *                 actions used to make connectorLock work.
 */

module.exports = require('connectorLock').connectorLocked({
  /* e.g.
    action: function(req, res){
  
    }
  */

});