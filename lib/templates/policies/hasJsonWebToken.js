'use strict';
/* jshint unused:false */

/**
 * hasJsonWebToken
 *
 * @module      :: Policy
 * @description :: Assumes that your request has an jwt;
 */
module.exports = function(req, res, next) {
  connectorLock.validator.validateTokenRequest(req, function(err, user){
    if(err){
      return res.forbidden(err);  
    }

    // valid request
    next();
  });
};
