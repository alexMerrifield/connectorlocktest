'use strict';

var ConnectorLock = require('./connectorLock');

// check if connectorLock is already attached
// if not create a new instance 
if(!global.hasOwnProperty('connectorLock')){
  global.connectorLock = new ConnectorLock(); 
}

// EXPORT IT!
module.exports = global.connectorLock;