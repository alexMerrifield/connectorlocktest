'use strict';

/* global connectorLock */

/**
 * jwt action
 *
 * creates a new token if a session is authenticated
 *
 * GET /user/jwt
 */
module.exports = function(req, res){
  if(!req.session.authenticated && !connectorLock._utils.getAccessToken(req)){
    return res.forbidden('You are not authorized.');
  }

  if(typeof req.session.authenticated !== 'undefined') {

    var jwtData = connectorLock._utils.createJwt(req, res);

    Jwt.create({token: jwtData.token, uses: 0, owner: req.session.user.id}).exec(function(err){
      if(err){
        return res.serverError('JSON web token could not be created');
      }

      var result = {};

      result[connectorLock.config.jsonWebTokens.tokenProperty] = jwtData.token || 'token';
      result[connectorLock.config.jsonWebTokens.expiresProperty] = jwtData.expires || 'expires';

      if (connectorLock.config.jsonWebTokens.includeUserInJwtResponse) {
        result['user'] = req.session.user;
      }

      res.json(result);
    });
    
  } else {
    connectorLock.validator.validateTokenRequest(req, function(err, user){
      if(err){
        return res.forbidden(err);
      } else {
        res.json(user);
      }
    });
  }
};