'use strict';

/**
 * logout action
 * 
 * creates a new token 
 * 
 * GET /user/jwt
 */
module.exports = function(req, res){
  var params = connectorLock._utils.allParams(req);

  if(typeof params.type === 'undefined' || 
    !connectorLock.methods.hasOwnProperty(params.type)){
    connectorLock.cycle.logout(req, res);
  }else{
    connectorLock.methods[params.type].actions.logout(req, res);
  }
};