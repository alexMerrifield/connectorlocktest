'use strict';

/**
 * login action
 *
 * tries to find if we have an auth method to handle this type of login
 * request.
 * 
 * GET /auth/login
 */
module.exports = function(req, res){
  var params = connectorLock._utils.allParams(req);

  // If there is only 1 chosen auth method just assume it
  if(connectorLock._utils.countTopLevel(connectorLock.methods) === 1){
    params.type = connectorLock._utils.accessObjectLikeArray(0, connectorLock.methods).authType;
  }

  if(typeof params.type === 'undefined'){
    return res.badRequest('You must specify a type parameter.');
  }

  if(connectorLock.methods.hasOwnProperty(params.type)){
    // call the login function of the correct auth type
    connectorLock.methods[params.type].actions.login(req, res);
  }else{
    return res.badRequest('unknown/invalid authentication type');
  }
};