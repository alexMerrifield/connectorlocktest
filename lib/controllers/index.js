'use strict';

var _ = require('lodash');

module.exports = function(){
  var connectorLock = this;

  return {
    /**
     * connectorLocked is an alias to the auth controller, connectorLocked just
     * sounds cooler. Bootstraps all extra actions from selected authentication
     * methods.
     *
     * @param  {object} actions user defiend actions
     * @return {object} user actions merged with template
     */
    connectorLocked: function(actions){
      var methods = connectorLock.methods;

      var actionTemplate = {};
      _.each(methods, function(method, key){
        var action = method.actions;
        if(_.has(action, 'extras')){
          connectorLock.logger.verbose('bootstraping auth actions from '+key);
          _.merge(actionTemplate, action.extras);
        }
      });

      var template = {
        login: require('./actions/login'),
        register: require('./actions/register'),
        logout:require('./actions/logout')
      };

      _.merge(actionTemplate, template);

      return _.merge(actionTemplate, actions);
    },

    /**
     * bootstraps user defined overrides with template actions
     *
     * @param  {object} actions user defiend actions
     * @return {object} user actions merged with template
     */
    user: function(actions){
      connectorLock.logger.verbose('bootstraping user actions');

      var template = {
        jwt: require('./actions/jwt')
      };
     return _.merge(template, actions);
    }
  };
};
