'use strict';

var path = require('path');

/**
 * Tries to require the connectorLock config
 * 
 * @return {object} connectorLock config file
 */
module.exports = function(){
  var config;
  try{
    var configPath = path.normalize(__dirname+'/../../../config/connectorLock.js');
    config = require(configPath).connectorLock;
  }catch(e){
    var error = new Error('No config file defined, try running [connectorLock generate config]\n\n'+e);
    throw error;
  }

  return config;
};