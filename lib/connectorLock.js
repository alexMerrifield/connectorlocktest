'use strict';

var _ = require('lodash');
var util = require('util');
var events = require('events');

module.exports = ConnectorLock;

/**
 * Creates a connectorLock instance
 */
function ConnectorLock(){
  events.EventEmitter.call(this);

  this.sails      = global.sails; 

  this.engine     = _.bind(this.engine, this)();
  this.config     = _.bind(this.config, this)();
  this.methods    = _.bind(this.methods, this)().collect();
  this.models     = _.bind(this.models, this)();
  this.actions    = _.bind(this.actions, this)();
  this.cycle      = _.bind(this.cycle, this)();

  // expose jwt so the implementing 
  // app doesn't need to require it.
  this.jwt        = require('jwt-simple');

  this.validator  = _.bind(this.validator, this)();
}

util.inherits(ConnectorLock, events.EventEmitter);

ConnectorLock.prototype.__defineGetter__('Auth', function(){
  return this.sails.models['auth'];
});

ConnectorLock.prototype.__defineGetter__('User', function(){
  return this.sails.models['user'];
});

ConnectorLock.prototype.__defineGetter__('Attempt', function(){
  return this.sails.models['attempt'];
});

ConnectorLock.prototype.__defineGetter__('Use', function(){
  return this.sails.models['use'];
});

ConnectorLock.prototype.__defineGetter__('Jwt', function(){
  return this.sails.models['jwt'];
});

ConnectorLock.prototype.__defineGetter__('connectorLocked', function(){
  return this.actions.connectorLocked;
});

ConnectorLock.prototype.engine = require('./engine');

ConnectorLock.prototype.config = require('./config');

ConnectorLock.prototype.methods = require('./methods');

ConnectorLock.prototype.models = require('./models');

ConnectorLock.prototype.actions = require('./controllers');

ConnectorLock.prototype._utils = require('./utils');

ConnectorLock.prototype.logger = require('./logger');

ConnectorLock.prototype.cycle = require('./cycle');

ConnectorLock.prototype.validator = require('./validator');