'use strict';

var proxyquire = require('proxyquire');
var should = require('should');
var mocha = require('mocha');
var config = require('./connectorLock.config');

var ConnectorLock =  proxyquire.noCallThru().load('../../lib/connectorLock',{
  './engine': function(){},
  './config': function(){},
  './methods': function(){return {collect: function(){}};},
  './controllers': function(){return{connectorLocked:{}};},
  './utils': {},
  './logger': {},
  './cycle': function(){}
});

describe('connectorLock', function(){
  it('should be a function', function(done){
    ConnectorLock.should.be.type('function');
    done();
  });

  describe('#constructor()', function(){
    it('should return a connectorLock object', function(done){
      global.sails = {};
      ConnectorLock.bind(this);
      var connectorLock = new ConnectorLock();
      connectorLock.should.be.instanceOf(ConnectorLock);
      done();
    });
  });

  describe('getters', function(){
    global.sails = {
      models:{
        auth: {},
        user: {},
        attempt: {},
        use: {},
        jwt: {}
      }
    };
    ConnectorLock.bind(this);
    var connectorLock = new ConnectorLock();
    it('should have an Auth', function(done){
      connectorLock.Auth.should.be.ok;
      done();
    }); 
    it('should have a User', function(done){
      connectorLock.User.should.be.ok;
      done();
    }); 
    it('should have an Attempt', function(done){
      connectorLock.Attempt.should.be.ok;
      done();
    });
    it('should have a Use', function(done){
      connectorLock.Use.should.be.ok;
      done();
    });
    it('should have an Jwt', function(done){
      connectorLock.Jwt.should.be.ok;
      done();
    });
    it('should have connectorLocked', function(done){
      connectorLock.connectorLocked.should.be.ok;
      done();
    }); 
  });
  describe('properties', function(){
    global.sails = {};
    ConnectorLock.bind(this);
    var connectorLock = new ConnectorLock();
    describe('.engine', function(){
      it('should exist', function(done){
        connectorLock.should.have.property('engine');
        done();
      });
    });
    describe('.config', function(){
      it('should exist', function(done){
        connectorLock.should.have.property('config');
        done();
      });
    });
    describe('.methods', function(){
      it('should exist', function(done){
        connectorLock.should.have.property('methods');
        done();
      });
    });
    describe('.models', function(){
      it('should exist', function(done){
        connectorLock.should.have.property('models');
        done();
      });
    });
    describe('.actions', function(){
      it('should exist', function(done){
        connectorLock.should.have.property('actions');
        done();
      });
    });
    describe('._utils', function(){
      it('should exist', function(done){
        connectorLock.should.have.property('_utils');
        done();
      });
    });
    describe('.logger', function(){
      it('should exist', function(done){
        connectorLock.should.have.property('logger');
        done();
      });
    });
    describe('.cycle', function(){
      it('should exist', function(done){
        connectorLock.should.have.property('cycle');
        done();
      });
    });
  });
});