'use strict';

var proxyquire = require('proxyquire');
var should = require('should');
var mocha = require('mocha');

describe('templates', function(){
  var auth = proxyquire.noCallThru().load('../../../../lib/templates/controllers/AuthController.js',
    {'connectorLock': {connectorLocked: function(){return {};}}});

  describe('controllers', function(){
    describe('AuthController', function(){
      it('should call connectorLocked', function(done){
        auth.should.be.Object;
        done();
      });
    });    
  });
});